import * as firebase from 'firebase'

export const state = () => ({
  categories: null,
  faqs: null,
  jobs: null
})

export const mutations = {
  setLoadedJobs(state, payload) {
    state.jobs = payload
  },
  setLoadedCategories(state, payload) {
    state.categories = payload
  },
  setLoadedFaqs(state, payload) {
    state.faqs = payload
  },
}

export const actions = {
  async loadJobs({
    commit
  }) {
    await firebase.database().ref('jobs').once('value')
      .then((data) => {
        const jobs = []
        const obj = data.val()
        for (let key in obj) {
          jobs.push({
            id: obj[key].id,
            title: obj[key].title,
            where: obj[key].where,
            category: obj[key].category,
            jobDescription: obj[key].jobDescription,
            whoAreYou: obj[key].whoAreYou,
            postDate: obj[key].postDate,
            employmentType: obj[key].employmentType
          })
        }
        commit('setLoadedJobs', jobs)
      })
      .catch(
        (error) => {
          console.log(error)
        }
      )
  },
  loadCategories({
    commit
  }) {
    firebase.database().ref('categories').once('value')
      .then((data) => {
        const categories = []
        const obj = data.val()
        for (let key in obj) {
          categories.push({
            id: key,
            name: obj[key].name,
            img: obj[key].img,
            description: obj[key].description
          })
        }
        commit('setLoadedCategories', categories)
      })
      .catch(
        (error) => {
          console.log(error)
        }
      )
  },
  loadFaqs({
    commit
  }) {
    firebase.database().ref('faqs').once('value')
      .then((data) => {
        const faqs = []
        const obj = data.val()
        for (let key in obj) {
          faqs.push({
            id: key,
            question: obj[key].question,
            answer: obj[key].answer
          })
        }
        commit('setLoadedFaqs', faqs)
      })
      .catch(
        (error) => {
          console.log(error)
        }
      )
  }
}
export const getters = {
  jobs(state) {
    return state.jobs
  },
  categories(state) {
    return state.categories
  },
  faqs(state) {
    return state.faqs
  },
  jobById(state) {
    return (jobId) => {
      return state.jobs.find((job) => {
        return job.id === jobId
      })
    }
  },
  jobsByCategory(state) {
    return (category) => {
      if (!category) {
        return []
      }
      return state.jobs.filter(job => job.category === category.name)
    }
  }
}
