// import colors from 'vuetify/es5/util/colors'

var foo = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: "Rappidoo Consulting",
    meta: [{
      charset: 'utf-8'
    }, {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    }, {
      property: "og:type",
      content: "website"
    }, {
      property: "twitter:card",
      content: "summary_large_image"
    }],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/logo.png'
    }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },
  /*
   ** Global CSS
   */
  css: [
    // CSS file in the project
    '@assets/css/main.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/dotenv',
    ['nuxt-material-design-icons'],
    ['@nuxtjs/firebase',
      {
        config: {
          apiKey: "AIzaSyBL-RRGtGYJS7S_AcY6xRFBQa-2kwVB8QY",
          authDomain: "rappidoo-consulting.firebaseapp.com",
          databaseURL: "https://rappidoo-consulting.firebaseio.com",
          projectId: "rappidoo-consulting",
          storageBucket: "rappidoo-consulting.appspot.com",
          messagingSenderId: "1031641798785",
          appId: "1:1031641798785:web:84bcbbe1bd3f7fad579580",
          measurementId: "G-GV4D9PHJTD"
        },
        services: {
          auth: true // Just as example. Can be any other service.
        }
      }
    ],
    ['@nuxtjs/google-analytics', {
      id: 'UA-158930920-1'
    }]
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** Build configuration
   */
  // 
  // build: {
  //   extend(config, {
  //     isDev,
  //     isClient,
  //     isServer
  //   }) {
  //     if (isServer) {
  //       config.externals = {
  //         '@firebase/app': 'commonjs @firebase/app',
  //         '@firebase/firestore': 'commonjs @firebase/firestore',
  //         //etc...
  //       }
  //     }
  //   }
  // }

  extend(config, ctx) {}

}

module.exports = foo
